var Reader = require("edifact/reader.js");
const fs = require("fs");

//var doc = new File('REMADV.edi');
//var doc = readTextFile('REMADV.edi');

fs.readFile("input/input.edi", "utf8", (err, data) => {
  if (err) {
    console.error(err);
    return;
  } else {
    //console.log(data.toString());
    let reader = new Reader({ autoDetectEncoding: true });
    let result = reader.parse(data.toString());

    // TO JSON
    //console.log(result);
    fs.writeFile("output/result.json", JSON.stringify(result), (err) => {
        if (err)
          console.log(err);
        else {
          console.log("JSON written successfully\n");
        }
      });

    // TO XML
    var convert = require('xml-js');
    var options = {compact: true, ignoreComment: true, spaces: 4};
    var xml = convert.json2xml(result, options);
    //console.log(xml);
    fs.writeFile("output/result.xml", xml, (err) => {
        if (err)
          console.log(err);
        else {
          console.log("XML written successfully\n");
        }
      });
  }
});